const math = require('mathjs');
const fs = require('fs');
math.config({
    number: 'BigNumber', // Default type of number:
    precision: 30 // Number of significant digits for BigNumbers
})
var Perceptron = require('./modelo/Perceptron');

function guess(argv){

    let {archivo,entrada,precision} = argv;

    let rawdata = fs.readFileSync(archivo);
    rawdata = JSON.parse(rawdata);

    let {perceptron,normalizador} = rawdata;
    normalizador = math.bignumber(normalizador);
    entrada = entrada.map((element)=>(math.divide(math.bignumber(element),normalizador)));

    perceptron = Perceptron.fromConf(perceptron);

    let resultado = perceptron.calculo(entrada);
    resultado = math.multiply(resultado,normalizador);
    resultado = math.round(resultado,precision);
    console.log(resultado);
}
module.exports = {
    guess
}
