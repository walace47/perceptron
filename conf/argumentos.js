const optGuess = {
    archivo: {
        alias:'a',
        describe:'ubicacion archivo de lectura',
        demand: true,
        demand: 'archivo es requerido'
        
    },
    entrada: {
        alias:'e',
        type:'array',
        describe:'Valores de entrada'
        
    },
    precision:{
        alias:'p',
        type:'number',
        describe:'Cantidad de decimales',
        default:0
    }
}

const optTrain = {
    problema:{
        alias:'p',
        default:1,
        describe:'Numero de problema'
    },
    tiempo:{
        alias:'t',
        default: 20000,
        describe:'Tiempo maximo de entrenamiento en Milisegundos'
    },
    nombre:{
        alias:'n',
        describe:'Nombre del archivo salida'
    },
    archivo:{
        alias:'a',
        describe:'Nombre del archivo de entrenamiento'
    }
}


const argv = require('yargs')
    .command('entrenar', 'entrenar perceptron', optTrain)
    .command('adivinar', 'predecir resultado', optGuess)
    .help()
    .argv;

module.exports = {
    argv
} 
