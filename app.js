const { argv } = require('./conf/argumentos');
const {entrenar} = require('./train');
const {guess} = require('./guess');


switch (argv._[0]) {
    case 'entrenar':
        entrenar(argv);
        break;
    case 'adivinar':
        guess(argv);
        break
    default:
        console.log('comando no reconocido')
        break;
}
