const fs = require('fs');
var Perceptron = require('./modelo/Perceptron');
//libreria de matematica
const math = require('mathjs');
//libreria de utilidades para el proyecto
const util = require('./funciones/funciones.js');
//liberia para el manejo de argumentos

function entrenar(argv){

    let {problema,tiempo,nombre,archivo} = argv;
    let parar = false;
    let dataSet;
    let par = [];
    if(problema){
        dataSet = util.obtenerDataSet(problema);
        if (dataSet.length > 0){
            par = util.estandarizarEntrada(dataSet);
        }else{
            console.error(`El problema ${problema} no tiene datos`);
            return
        }

    }

    if(archivo){
        dataSet = fs.readFileSync(archivo);
        par = JSON.parse(dataSet);
    }
    par = util.normalizar(par);
    let normalizador = par[0].normalizador;
    let res = 0;
    let errTmp = 100;
    p = new Perceptron(par[0].inputs.length);

    let tiempoInicial = (new Date()).getTime();
    let tiempoFinal = tiempoInicial + tiempo;


    while (errTmp != 0 &&  (tiempoInicial) < tiempoFinal  ) {

        errTmp = math.bignumber(0);

        for (let i = 0; i < par.length/2; i++) {
        let errorEntrenamiento = math.abs(p.train(par[i].inputs, par[i].output));
            errTmp = math.add( errTmp, errorEntrenamiento);

        }

        errTmp = math.divide(errTmp, par.length/2 );

        if ( errTmp < 0.0000001) {
            errTmp = 0;
            tiempoInicial = tiempoFinal;
        }
        res++;
        tiempoInicial = (new Date()).getTime();
    }
    let data = {
        error:errTmp,
        epocas:res,
        perceptron:p.getConf(),
        normalizador:par[0].normalizador
    }

    data = JSON.stringify(data,null,2);
	nombre = nombre || `ej${problema}.json`;
    let nombreArchivo = nombre;
    fs.writeFile(nombreArchivo, data,'utf8', (err) => {
        if (err) throw err;
        console.log(`El archivo ${nombreArchivo} fue creado con exito`);
    });

}

module.exports = {
    entrenar
}
