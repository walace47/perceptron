### Instalación 
Una vez descargado o clonado el stub ir al directorio donde lo descargaste abrir una consola de comandos y ejecutar:

```
npm install
```

Luego modificar el archivo app.js.

### Ejecucion

Una vez instaladas las dependencias y modificado el archivo app.js ejecutar:

```
node app.js
```
